

var https    = require('https');
var Constant = require('./constant');

var data_path = './data';

function DataUtils() {
   this.json = {};
};

DataUtils.prototype.init = function() {
    var self = this;
    try {
        self.json = require(data_path);
        return true;
    } 
    catch (err) { 
        console.log("error loading " + data_path + "...\n > " + err); 
        return false;
    }
}

DataUtils.prototype.getNearest = function(lat, lon) {
    return JSON.parse(JSON.stringify(this.json.hospitals));
}

DataUtils.prototype.getDistance = function(lat, lon, callback) {

    var str_path = "";
    var str_dest = "";
    var nearest  = this.getNearest(lat, lon);
    // console.log("get distance: " + JSON.stringify(nearest,  null, "    ")); 

    if (nearest && nearest.length > 0) {
        nearest.forEach(hospital => {
            if (str_dest.length > 0)
                str_dest += '%7C';
            str_dest += hospital.lat + '%2C' + hospital.lon;
        });
    }
    str_path = 'origins=' + lat + ',' + lon + '&destinations=' + str_dest + '&key=' + Constant.GOOGLE_MAP_KEY;

    console.log("str url: '" + Constant.API_MAPS_URL + "'"); 
    console.log("str path: '" + str_path + "'"); 

    var options  = {
        hostname: Constant.API_MAPS_URL,
        method:   "GET",
        path:     Constant.API_MAPS_PATH + str_path
    };

    // https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=3.055549,101.701008&destinations=3.1000331%2C101.730533%7C3.104187%2C101.745296%7C3.045307%2C101.720749&key=AIzaSyCGgx5j_zAXrqZFLtdsqQXpltEXp_tXVgs

    var req = https.get(options, function(response) {

        // Continuously update stream with data
        var body      = '';
        var json_body = {};
        var elements  = {};
        var count = 0;
        var i     = 0;
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            // console.log("distance: " + body); 
            if (body) {
                json_body = JSON.parse(body);
                if (json_body) {
                    if (json_body.destination_addresses) {
                        for (i = 0; i < json_body.destination_addresses.length; i++) {
                            nearest[i]['address'] = json_body.destination_addresses[i];
                        }
                    }

                    if (json_body.rows && json_body.rows.length > 0 && json_body.rows[0].elements) {
                        elements = json_body.rows[0].elements;
                        for (i = 0; i < elements.length; i++) {
                            nearest[i]['distance'] = elements[i].distance.value;
                            nearest[i]['duration_secs'] = elements[i].duration.value;

                        }                           
                    }
                }
            }
            if (callback)
                callback(response.statusCode, nearest);
        });
        response.on('error', function(error) {
            if (callback)
                callback(response.statusCode, error);
        });
    });
    req.on('error', function(error) {
        if (callback)
            callback(-1, { "error": error });
    });
}

module.exports = DataUtils;