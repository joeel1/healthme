var https    = require('https');
var jwt      = require('jsonwebtoken');
var Constant = require('./constant');

// var hostname        = "https://www.googleapis.com";
// var path_v1_certs   = "/oauth2/v1/certs";
//var google_v1_certs = "https://www.googleapis.com/oauth2/v1/certs";
var certs           = {};

function GoogleSignInUtil() {
}


GoogleSignInUtil.prototype.decode = function(id_token) {
    var ret    = {};
    var tokens = id_token.split('.');
    
    
    if (tokens.length >= 3) {
        var header  = {};
        var payload = {};
        
        try { header  = JSON.parse(new Buffer(tokens[0], 'base64')); } catch (e) {}
        try { payload = JSON.parse(new Buffer(tokens[1], 'base64')); } catch (e) {}
        
        for (key in header) {
            ret[key] = header[key];
        }
        for (key in payload) {
            ret[key] = payload[key];
        }
    }
    
    return ret;
}

GoogleSignInUtil.prototype.verify = function(token, callback) {
    var decoded = this.decode(token);

    getCert(decoded.kid, function(cert){
        if (cert) {
            var error   = null;
            var results = null;
            try {
                results = jwt.verify(token, cert, { algorithms: [decoded.alg] })
            }
            catch (e) { error = e; }

            if (callback)
                callback(error, results);
        }
    });
}


function getCert(kid, callback) {
    var cert = certs[kid];
    if (cert == undefined) {
        console.log(">>>> cert fetcher: cert '" + kid + "' unavailable. refreshing cert list ");
        downloadCerts(function() {
            cert  = certs[kid];
            if (callback)
                callback(cert);
        });
    }
    else if (callback) {
        console.log(">>>> cert fetcher: cert '" + kid + "' found");
        callback(cert);
    }
}


function downloadCerts(callback) {
    https.get(Constant.API_GOOGLE + Constant.GOOGLE_CERT_PATH, function(response) {
//        console.log('statusCode: ', response.statusCode);
//        console.log('headers: ', response.headers);

        var body = '';
        response.on('data', function(data){
            body += data;
        });
        response.on('end', function(data){
            certs = JSON.parse(body);
            if (callback)
                callback();
        });
    }).on('error', function(e) {
        console.log(">>>> cert fetcher: error " + JSON.stringify(e, null, "    "));
    });
}

module.exports = GoogleSignInUtil;
