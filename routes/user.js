
var express          = require('express');
var GoogleSignInUtil = require('../google_signin_utils');
var Constant         = require('../constant');

var router           = express.Router();
var singin_util      = new GoogleSignInUtil();




router.get('/', function (req, res) {
    var str  = "User interface:\n\n"
    res.setHeader("content-type", "text/plain");
    res.end(str);
});


router.get('/nearest', function(req, res, next) {
    res.setHeader("content-type", "application/json");
    if (router.data_utils) {
        res.json(router.data_utils.getNearest('3.055549', '101.701008'));
    } else {
        res.status(404);
        res.json({ error: "data unavailable..." });
    }
    res.end();  
});


router.get('/distance', function(req, res, next) {
        var lat   = req.query.lat;
        var lon   = req.query.lon;
        var token = req.query.token;

        // console.log(">>>> get distance: lat '" + lat + "'");
        // console.log(">>>> get distance: lon '" + lon + "'");
        // console.log(">>>> get distance: token '" + token + "'");
    processToken(req, res, function() {
    
        res.setHeader("content-type", "application/json");
        if (router.data_utils) {
            router.data_utils.getDistance(lat, lon, function(status_code, json) {
                res.json(json);
                res.end();  
            });
        } else {
            res.status(404);
            res.json({ error: "data unavailable..." });
            res.end();  
        }
    
    });
});

function processToken(request, response, onValidToken){
    var token = "";
    if (request.query.token)
        token = request.query.token;

    if (token == null || token.length <= 0) {
        response.status(404);
        response.setHeader("content-type", "application/json");
        response.json({ error: "id token not provided" });
        response.end();
        return;
    }

    singin_util.verify(token, function(error, results) {
        var error_msg = null;
        if (results) {
            // console.log(">>>> process token: token  '" + JSON.stringify(results, null, "    ") + "'");
            //console.log(">>>> process token: token verified and have same project id");
            var i          = 0;
            var flag_valid = false;
            if (results.aud == Constant.WEB_CLIENT_ID) {
                console.log(">>>> process token: token verified for project id '" + Constant.WEB_CLIENT_ID + "'");
                flag_valid = true;
            }
            if (flag_valid) {
                if (onValidToken)
                    onValidToken();
            }
            else
                error_msg = "application's id doesnt matched!...";
        }
        else
            error_msg = JSON.stringify(error, null, "    ");

        if (error_msg) {
            console.log(">>>> process token: token error - " + error_msg);
            response.status(403);
            response.setHeader("content-type", "application/json");
            response.json({ error: error_msg });
            response.end();
        }
    });
}



module.exports = router;
